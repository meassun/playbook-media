import React, { Component } from 'react';
import desktopImage from "assets/img/754632.jpg";
import mobileImage from "assets/img/754632.jpg";
import "assets/css/lockscreen.css"
import {
    Row,
    Col,
    FormGroup,
    Input,
    Button
} from "reactstrap";
class Lock extends Component {
    state = {
        isPasswordShown: false
    };

    togglePasswordVisibility = () =>{
        const {isPasswordShown} = this.state;
        this.setState({ isPasswordShown: !isPasswordShown });
    }
    render() {
        const imageUrl = window.innerWidth >= 650 ? desktopImage : mobileImage;
        const { isPasswordShown } = this.state;
        return (
            <div className="lock-screen" style={{ backgroundImage: `url(${imageUrl})` }}>
                <div className="lock-content">
                    <Row>
                        <Col md="4"></Col>
                        <Col md="4">
                        <div class="user-thumb">
                        <img
                                alt="..."
                                className="avatar border-gray"
                                src={require("assets/img/profile_account.jpg")}
                            />
                            <h3>Meas Sun</h3>
                        </div>
                        <div class="form-group">
                            <FormGroup>
                                <Input
                                    placeholder="Password"
                                    type={isPasswordShown ? "text" : "password"}
                                />
                                <i class={`fa ${isPasswordShown ?  "fa-eye" : "fa-eye-slash"} password-icon`} onClick={this.togglePasswordVisibility} />
                            </FormGroup>
                        </div>
                        <div class="text-right">
                            <a href="#" class="text-muted">Forget lock pin ?</a>
                        </div>
                        <Row>
                                <div className="update ml-auto mr-auto">
                                    <a href="/home">
                                        <Button
                                            className="btn btn-primary btn-wd"
                                            color="primary"
                                            type="submit"
                                        >
                                            <span class="fa fa-unlock"></span> Unlock
                                            </Button>
                                    </a>
                                </div>
                            </Row>
                        
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default Lock;