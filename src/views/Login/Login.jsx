/*!

=========================================================
* Page Admin
=========================================================

*/
import React from "react";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";
import "assets/css/login.css"
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardTitle,
    Form,
    FormGroup,
    CardBody,
    Input, 
    Spinner
  } from "reactstrap";
  import { Redirect } from "react-router-dom";
  import { Button } from "reactstrap";
  import { login } from "../../redux/actions/UserAction";
  import { connect } from "react-redux";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
			password: "",
      isPasswordShown: false
    };
    this.mainPanel = React.createRef();
  }

  togglePasswordVisibility = () =>{
    const {isPasswordShown} = this.state;
    this.setState({ isPasswordShown: !isPasswordShown });
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onLogin = () => {
    const {email, password} = this.state;
    this.props.onLogin(email, password);
  }

  errorLogin = () => {
    const errormsg = <p className="text-danger">Username or password incorrect!</p>;
    return errormsg;
  }

  render() {  
    const style ={
      marginLeft: "0px",
      marginRight: "0px",
    }
    const { isPasswordShown } = this.state;
    const {email, password} = this.state;
    const { loading, isAuthenticated } = this.props;
    if (isAuthenticated) {
			return <Redirect to="/" />;
		}else{
      
    }
    return (
      <div className="wrapper">
        <div className="main-panel" ref={this.mainPanel}>
            <br/><br/>
            <br/><br/>
                <Row style={style}>
                    <Col md="4"></Col>
                    <Col md="4">
                        <Card className="card-user">
                            <CardHeader>
                                <CardTitle className="text-center" tag="h5"><b>Log In to Playbook</b></CardTitle>
                            </CardHeader>
                            <CardBody>
                                <Form>
                                    <Row>
                                        <Col md="12">
                                            <FormGroup>
                                                <label>Email</label>
                                                <Input
                                                    placeholder="Email address"
                                                    name="email"
                                                    type="text"
                                                    value={email}
                                                    onChange={this.onChange}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="12">
                                            <FormGroup>
                                                <label>Password</label>
                                                <Input
                                                    placeholder="Password"
                                                    name="password"
                                                    type={isPasswordShown ? "text" : "password"}
                                                    value={password}
                                                    onChange={this.onChange}
                                                />
                                                <i className={`fa ${isPasswordShown ?  "fa-eye" : "fa-eye-slash"} password-login-icon`} onClick={this.togglePasswordVisibility} />

                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <div className="text-right">
                                        <a href="/forget-pass" className="text-muted">Forget your password ?</a>
                                    </div>
                                    
                                    <Row>
                                        <div className="update ml-auto mr-auto">
                                          {loading ? (
                                            <Spinner animation="grow" variant="danger" />
                                          ) : (
                                            <Button className="btn btn-primary btn-wd"
                                            color="primary"
                                            onClick={this.onLogin}>Login</Button>
                                          )}

                                        </div>
                                    </Row>

                                    <Row>
                                        <div className="update ml-auto mr-auto" id="error-login">

                                        </div>
                                    </Row>
                                </Form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.user.loading,
    token: state.user.token,
    isAuthenticated: state.user.token != null
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLogin: (email, password) => dispatch(login(email, password))
    
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
