import React from "react";

import { Formik } from "formik";
import * as EmailValidator from "email-validator";
import * as Yup from "yup";
import "assets/css/login.css"
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardTitle,
    Form,
    FormGroup,
    CardBody,
    Input
  } from "reactstrap";
  
import { Button } from "reactstrap";

const ValidateLoginForm = () => (
  
  <Formik
    initialValues={{ email: "", password: "" }}
    onSubmit={(values, { setSubmitting }) => {
      setTimeout(() => {
        console.log("Logging in", values);
        setSubmitting(false);
      }, 500);
    }}

    //********Using Yum for validation********/

    validationSchema={Yup.object().shape({
      email: Yup.string()
        .email()
        .required("Required"),
      password: Yup.string()
        .required("No password provided.")
        .min(8, "Password is too short - should be 8 chars minimum.")
        .matches(/(?=.*[0-9])/, "Password must contain a number.")
    })}
  >
    
    {props => {
      const {
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit,
      } = props;
      const style ={
        marginLeft: "0px",
        marginRight: "0px",
      }
      // const { isPasswordShown } = this.state;
      
      // let togglePasswordVisibility = () =>{
      //   const {isPasswordShown} = this.state;
      //   this.setState({ isPasswordShown: !isPasswordShown });
      // }
      
      return (
        
        <div className="wrapper">
          <div className="main-panel">
            <br /><br />
            <br /><br />
            <Row style={style}>
              <Col md="4"></Col>
              <Col md="4">
                <Card className="card-user">
                  <CardHeader>
                    <CardTitle className="text-center" tag="h5"><b>Log In to Ticket System</b></CardTitle>
                  </CardHeader>
                  <CardBody>
                    <Form onSubmit={handleSubmit}>
                      <label htmlFor="email">Email</label>
                      <Input
                        name="email"
                        type="text"
                        placeholder="email address"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.email && touched.email && "error"}
                      />
                      {errors.email && touched.email && (
                        <span style={{color: 'red', fontSize: 11}} className="input-feedback">{errors.email}</span>
                      )}
                      <br/>
                      <label htmlFor="email">Password</label>
                      <Input
                        name="password"
                        // type={isPasswordShown ? "text" : "password"}
                        type={false ? "text" : "password"}
                        placeholder="password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={errors.password && touched.password && "error"}
                      />
                      <i className={`fa ${false ?  "fa-eye" : "fa-eye-slash"} password-login-icon`} />
                      {/* <i class={`fa ${false ?  "fa-eye" : "fa-eye-slash"} password-login-icon`} onClick={togglePasswordVisibility} /> */}
                      {errors.password && touched.password && (
                        <span style={{color: 'red', fontSize: 11}} className="input-feedback">{errors.password}</span>
                      )}
                      <br/>
                      <Button 
                        className="btn btn-primary btn-wd"
                        color="primary"
                        type="submit" disabled={isSubmitting}>
                        Login
                      </Button>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        </div>


      );
    }}
  </Formik>
);

export default ValidateLoginForm;
