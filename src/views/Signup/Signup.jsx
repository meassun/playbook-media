/*!

=========================================================
* Page Admin
=========================================================

*/
import React from "react";
// javascript plugin used to create scrollbars on windows
import "assets/css/signup.css"
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardTitle,
    Form,
    FormGroup,
    CardBody,
    Input,
    Spinner
  } from "reactstrap";
  
  import { Button } from "reactstrap";
  import { connect } from 'react-redux';
  import { isUserLogin } from '../../redux/actions/UserAction';
  import { signup } from "../../redux/actions/UserAction";
  import { Redirect } from 'react-router-dom';

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      conpassword: "",
      gender: "",
      birthdate: "",
      isPasswordShown: false
    };
    this.mainPanel = React.createRef();
    this.props.checkIfUserLogin();
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSignup = () => {
    const {firstname, lastname, email, password, conpassword, gender, birthdate} = this.state;
    console.log(firstname, lastname, email, password, conpassword, gender, birthdate);
    this.props.onSignup(firstname, lastname, email, email, password, conpassword, gender, birthdate);
  }

  togglePasswordVisibility = () =>{
    const {isPasswordShown} = this.state;
    this.setState({ isPasswordShown: !isPasswordShown });
  }
  render() {
    const {isPasswordShown} = this.state;
    const {firstname, lastname, email, password, conpassword, gender, birthdate} = this.state;
    const { loading, isAuth } = this.props;
    
    if(isAuth){
      return <Redirect to="/" />;
    }

    if(this.props.isAuthenticated){
      return <Redirect to="/" />;
    }
    
    return (
      <div className="wrapper">
        <div className="main-panel" ref={this.mainPanel}>
            <br/><br/>
            <br/><br/>
              <Row style={{marginLeft: "0px", marginRight: "0px",}}>
              <Col md="4"></Col>
              <Col md="4">
              <Card className="card-user">
                <CardHeader>
                  <CardTitle className="text-center" tag="h5"><b>Sign Up New Account</b></CardTitle>
                  <div className="text-center social">
                    
                    <button className="btn-icon btn-round btn btn-fb">
                      <i className="fab fa-facebook-f pr-1"></i>
                    </button>
                    <button className="btn-icon btn-round btn btn-tw">
                      <i className="fab fa-twitter"></i>
                    </button>
                    <button className="btn-icon btn-round btn btn-gplus">
                      <i className="fab fa-google-plus-g"></i>
                    </button>
                    <p className="card-description">Sign up with social media</p>
                  </div>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <label>First Name</label>
                          <Input
                            placeholder="Enter First name"
                            name="firstname"
                            type="text"
                            value={firstname}
                            onChange={this.onChange}
                          />
                        </FormGroup>
                      </Col>
                      <Col md="6">
                        <FormGroup>
                          <label>Last Name</label>
                          <Input
                            placeholder="Enter Last Name"
                            name="lastname"
                            type="text"
                            value={lastname}
                            onChange={this.onChange}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Email</label>
                          <Input
                            placeholder="Enter email"
                            name="email"
                            type="text"
                            value={email}
                            onChange={this.onChange}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Password</label>
                          <Input
                            placeholder="Password"
                            name="password"
                            type={isPasswordShown ? "text" : "password"}
                            value={password}
                            onChange={this.onChange}
                          />
                          <i className={`fa ${isPasswordShown ? "fa-eye" : "fa-eye-slash"} password-signup-icon`} onClick={this.togglePasswordVisibility} />

                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Confirm Password</label>
                          <Input
                            placeholder="confirm password"
                            name="conpassword"
                            type={isPasswordShown ? "text" : "password"}
                            value={conpassword}
                            onChange={this.onChange}
                          />
                          <i className={`fa ${isPasswordShown ? "fa-eye" : "fa-eye-slash"} password-signup-icon`} onClick={this.togglePasswordVisibility} />

                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label htmlFor="birthday">Birthday</label>
                          <Input
                              type="date"
                              name="birthdate"
                              value={birthdate}
                              onChange={this.onChange}
                            />
                        </FormGroup>
                      </Col>
                    </Row>

                    <Row>
                    <Col md="12">
                      <FormGroup>
                        <label htmlFor="birthday">Gender</label>
                          <div className="col-md-12 gender-group" style={{marginLeft: "10%"}}>
                            <input type="checkbox" 
                            name="gender"
                            value={gender}
                            onChange={this.onChange}
                            />
                            &nbsp;&nbsp;

                            <span className="button-checkbox">Male</span>&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" />&nbsp;&nbsp;
                            <span className="button-checkbox">Female</span>&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" />&nbsp;&nbsp;
                            <span className="button-checkbox">Not sure</span>
                          </div>
                        </FormGroup>
                    </Col>
                    </Row>
                    
                    <Row>
                      <div className="update ml-auto mr-auto">
                        {loading ? (
                          <Spinner animation="grow" variant="danger" />
                        ) : (
                            <Button
                              className="btn-round"
                              color="primary"
                              onClick={this.onSignup}
                            >
                              Sign Up Now
                            </Button>
                          )}
                      </div>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.user.loading,
    token: state.user.token,
    isAuthenticated: state.user.token != null
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkIfUserLogin: () => dispatch(isUserLogin()),
    onSignup: (firstname, lastname,email, username, password, conpassword, gender, birthdate) => dispatch(signup(firstname, lastname, email, username, password, conpassword, gender, birthdate))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
