/*!

=========================================================
* Navbar
=========================================================

*/
import React from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  Container,
  InputGroup,
  InputGroupText,
  InputGroupAddon,
  Input,
  CardBody,
  Row,
  Card,
  Col,
  Button,
  CardFooter
} from "reactstrap";

import routes from "routes.js";
import { connect } from 'react-redux';
import { isUserLogin, logout } from "../../redux/actions/UserAction";
import swal from "sweetalert";
class MenuNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      dropdownNotificationOpen: false,
      dropdownSettingOpen: false,
      color: "transparent"
    };
    this.toggle = this.toggle.bind(this);
    this.dropdownNotification = this.dropdownNotification.bind(this);
    this.dropdownSetting = this.dropdownSetting.bind(this);
    this.sidebarToggle = React.createRef();

    this.props.checkIfUserLogin();
    this.onLogout = this.onLogout.bind(this);

  }
  toggle() {
    if (this.state.isOpen) {
      this.setState({
        color: "transparent"
      });
    } else {
      this.setState({
        color: "dark"
      });
    }
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  //New Post
  dropdownPost(e) {
    this.setState({
      dropdownPostOpen: !this.state.dropdownPostOpen
    });
  }
  //Notifications 
  dropdownNotification(e) {
    this.setState({
      dropdownNotificationOpen: !this.state.dropdownNotificationOpen
    });
  }
  // Accound setting dropdown
  dropdownSetting(e) {
    this.setState({
      dropdownSettingOpen: !this.state.dropdownSettingOpen
    });
  };

  // On Logout function 
  onLogout() {
    swal({
      title: "Are you sure to logout?",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then((willLogout) => {
      if (willLogout) {
        this.props.logout();

      }
    });
  }


  getBrand() {
    let brandName = "Home";
    routes.map((prop, key) => {
      if (window.location.href.indexOf(prop.layout + prop.path) !== -1) {
        brandName = prop.name;
      }
      return null;
    });
    return brandName;
  }
  openSidebar() {
    document.documentElement.classList.toggle("nav-open");
    this.sidebarToggle.current.classList.toggle("toggled");
  }
  // function that adds color dark/transparent to the navbar on resize (this is for the collapse)
  updateColor() {
    if (window.innerWidth < 993 && this.state.isOpen) {
      this.setState({
        color: "dark"
      });
    } else {
      this.setState({
        color: "transparent"
      });
    }
  }
  componentDidMount() {
    window.addEventListener("resize", this.updateColor.bind(this));
  }
  componentDidUpdate(e) {
    if (
      window.innerWidth < 993 &&
      e.history.location.pathname !== e.location.pathname &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
      this.sidebarToggle.current.classList.toggle("toggled");
    }
  }
  render() {
    const NavLoginStyle = {
      height: "100%",
      width: "0%",
      marginLeft: "55%"
    }
    const NavStyle = {
      height: "100%",
      width: "0%"
    }
    const NavLink = {
      display: "inline-flex",
      alignitems: "center"
    }
    const IconStyle = {
      fontSize: "20px"
    }
    return (
      <>
        <Navbar
          color={
            this.props.location.pathname.indexOf("full-screen-maps") !== -1
              ? "dark"
              : this.state.color
          }
          expand="lg"
          className={
            this.props.location.pathname.indexOf("full-screen-maps") !== -1
              ? "navbar-absolute fixed-top"
              : "navbar-absolute fixed-top " +
              (this.state.color === "transparent" ? "navbar-transparent " : "")
          }
        >
          <Container fluid>
            <div className="navbar-wrapper">
              <div className="navbar-toggle">
                <button
                  type="button"
                  ref={this.sidebarToggle}
                  className="navbar-toggler"
                  onClick={() => this.openSidebar()}
                >
                  <span className="navbar-toggler-bar bar1" />
                  <span className="navbar-toggler-bar bar2" />
                  <span className="navbar-toggler-bar bar3" />
                </button>
              </div>
              <NavbarBrand href="/home">Playbook media</NavbarBrand>

            </div>
            <NavbarToggler onClick={this.toggle}>
              <span className="navbar-toggler-bar navbar-kebab" />
              <span className="navbar-toggler-bar navbar-kebab" />
              <span className="navbar-toggler-bar navbar-kebab" />
            </NavbarToggler>
            {/* add or remove classes depending if user on login or not */}
            {this.props.isAuthenticated ? (
              <>
                <Collapse navbar className="justify-content-around nav" style={NavStyle}>
                  <Row>
                    <Col md="4">
                      <li className="navbar-item nav-item">
                        <a className="nav-link qa-test-home-link selected" aria-current="page" href="/home" style={NavLink}>
                          <i className="nc-icon nc-bank" style={IconStyle} />
                          <span className="nav-bar-label">&nbsp;&nbsp;Home</span>
                        </a>
                      </li>
                    </Col>
                    <Col md="4">
                      <li className="navbar-item nav-item">
                        <a className="nav-link qa-test-classes-link" href="/classes" style={NavLink}>
                          <i className="nc-icon nc-world-2" style={IconStyle} />
                          <span className="nav-bar-label">&nbsp;&nbsp;Discover</span>
                        </a>
                      </li>
                    </Col>
                    <Col md="4">
                      <li className="navbar-item nav-item">
                        <a className="nav-link qa-test-discover-link" href="/Messages" style={NavLink}>
                          <i className="nc-icon nc-chat-33" style={IconStyle} />
                          <span className="nav-bar-label">&nbsp;&nbsp;Messages</span>
                        </a>
                      </li>
                    </Col>
                  </Row>
                </Collapse>
                <Collapse
                  isOpen={this.state.isOpen}
                  navbar
                  className="justify-content-end"
                >

                  <form>
                    <InputGroup className="no-border">
                      <Input placeholder="Search..." />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>
                          <i className="nc-icon nc-zoom-split" />
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </form>
                  <Nav navbar>
                    <Dropdown
                      nav
                      isOpen={this.state.dropdownNotificationOpen}
                      toggle={e => this.dropdownNotification(e)}
                    >
                      <DropdownToggle nav>
                        <i className="nc-icon nc-bell-55" />
                        <p>
                          <span className="notification">5</span>
                          <span className="d-lg-none d-md-block">Notifications</span>
                        </p>
                      </DropdownToggle>
                      <DropdownMenu right>
                        <Col lg="12" md="6" sm="6">
                          <Card className="card-stats">
                            <CardBody>
                              <Row>
                              </Row>
                            </CardBody>
                            <CardFooter>
                              <i /> Notifications
                              <hr />

                            </CardFooter>
                          </Card>
                        </Col>
                      </DropdownMenu>
                    </Dropdown>

                    <Dropdown
                      nav
                      isOpen={this.state.dropdownSettingOpen}
                      toggle={e => this.dropdownSetting(e)}
                    >
                      <DropdownToggle nav>
                        <img
                          alt="..."
                          className="avatar border-gray"
                          src={require("assets/img/profile_account.jpg")}
                        />
                        <p>
                          <span className="d-lg-none d-md-block">Account Setting</span>
                        </p>
                      </DropdownToggle>
                      <DropdownMenu right>
                        <a className="dropdown-item" href="#">
                          <i className="nc-icon nc-email-85"></i> Profile
                              </a>
                        <a className="dropdown-item" href="#">
                          <i className="nc-icon nc-umbrella-13"></i> Help Center
                              </a>
                        <div className="divider"></div>
                        <a className="dropdown-item" href="#">
                          <i className="nc-icon nc-lock-circle-open"></i> Lock Screen
                              </a>
                          <Button
                            className="btn btn-primary btn-wd"
                            color="primary" onClick={this.onLogout}
                          >
                            Log out
                          </Button>
                      </DropdownMenu>
                    </Dropdown>
                  </Nav>
                </Collapse>
              </>
            ) : (
                <>
                  <Collapse  navbar className="justify-content-around nav" style={NavLoginStyle}>
                    <Nav navbar>
                      <li className="nav-item ">
                        <a href="/signup" className="nav-link">
                          <i className="nc-icon nc-badge"></i> Register
                              </a>
                      </li>
                      <li className="nav-item ">
                        <a href="/login" className="nav-link">
                          <i className="nc-icon nc-mobile"></i> Login
                              </a>
                      </li>
                      <li className="nav-item ">
                        <a href="/lock" className="nav-link">
                          <i className="nc-icon nc-key-25"></i> Lock
                            </a>
                      </li>
                    </Nav>
                  </Collapse>
                </>
            )}

          </Container>
        </Navbar>
      </>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.user.token != null
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkIfUserLogin: () => dispatch(isUserLogin()),
    logout: () => dispatch(logout())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuNavbar);
