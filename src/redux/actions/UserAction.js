import {User} from "../../constant/ActionType";
import {API} from "../../constant/API";
import axios from "axios";

const signupStart = () => {
    return {
        type: User.SIGNUP_START
    };
};

const signupSuccess = (data) => {
    return {
        type: User.SIGNUP_SUCCESS,
        data
    };
};

const signupFail = (error) => {
    return {
        type: User.SIGNUP_FAIL,
        error
    };
};

const loginStart = () => {
    return {
        type: User.LOGIN_START
    };
};

const loginSuccess = (data) => {
    return {
        type: User.LOGIN_SUCCESS,
        data
    };
};

const loginFail = (error) => {
    return {
        type: User.LOGIN_FAIL,
        error
    };
};

const checkIfUserLogin = (token) => {
    console.log("Check login: ",token);
    return {
        type: User.CHECK_LOGIN, 
        token
    }
}

const logoutStart = () => {
	return {
		type: User.LOGOUT
	};
};

export const login = (email, pass) => {
    return (dispatch) => {
        dispatch(loginStart());
        axios 
            .post(`${API.baseURL}/rest-auth/login/`, {
                username: email,
                password: pass
            })
            .then((res) => {
                localStorage.setItem("token", res.data.key);
                dispatch(loginSuccess(res.data));
            })
            .catch((err) => {
                dispatch(loginFail(err));
            });
    };
};

//Signup 
export const signup = (fname, lname, email, username, pass, conpass, gen, birth) => {
    return (dispatch) => {
        dispatch(signupStart());
        axios 
            .post(`${API.baseURL}/rest-auth/registration/`, {
                email : email,
                username: username,
                password1: pass,
                password2: conpass
            })
            .then((res) => {
                localStorage.setItem("token", res.data.key);
                dispatch(signupSuccess(res.data));
            })
            .catch((err) => {
                dispatch(signupFail(err));
            });
    }
}

export const isUserLogin = () => {
    return (dispatch) => {
        const token = localStorage.getItem("token");
        dispatch(checkIfUserLogin(token));
    };
};

export const logout = () => {
    return (dispatch) => {
        localStorage.removeItem("token");
        dispatch(logoutStart());
        console.log("Sign Out Successfully");
    };
};

