import {User} from "../../constant/ActionType";
import {updateObject} from "../../utilities/Utilities";

const initState = {
    loading: false,
    error: null,
    token: null, 
    user: null
};

const signupStart = (state) => {
    return updateObject(state, {
        loading: true,
        error: null
    });
}
const signupSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        token: action.data.key
    });
}
const signupFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        error: action.error
    });
}


const loginStart = (state, action) => {
    return updateObject(state, {
        loading: true,
        error: null
    });
};

const loginSuccess = (state, action) => {
	return updateObject(state, {
		loading: false,
		token: action.data.key
	});
};
const loginFail = (state, action) => {
	return updateObject(state, {
		loading: false,
		error: action.error
	});
};

const checkIfUserLogin = (state, action) => {
	return updateObject(state, {
		token: action.token
	});
};

const logoutStart = (state, action) => {
	return updateObject(state, {
		token: null
	});
};

export const userReducer = (state = initState, action) => {
    switch (action.type) {
        case User.SIGNUP_START:
            return signupStart(state);
        case User.SIGNUP_SUCCESS:
            return signupSuccess(state, action);
        case User.SIGNUP_FAIL:
            return signupFail(state, action);
        case User.LOGIN_START:
            return loginStart(state, action);
        case User.LOGIN_SUCCESS:
            return loginSuccess(state, action);
        case User.LOGIN_FAIL:
            return loginFail(state, action);
        case User.CHECK_LOGIN:
            return checkIfUserLogin(state, action);
        case User.LOGOUT:
            return logoutStart(state, action);
        default:
            return state;
    }
};