export const User = {
    LOGIN_START: "USER_START_LOGIN",
    LOGIN_SUCCESS: "USER_SUCCESS_LOGIN",
    LOGIN_FAIL: "USER_FAIL_LOGIN",
    CHECK_LOGIN: "USER_CHECK_LOGIN",
    LOGOUT: "USER_LOGOUT",
    
    //SIGNUP ACTION
    SIGNUP_START: "USER_START_SIGNUP",
    SIGNUP_SUCCESS: "USER_SUCCESS_SIGNUP",
    SIGNUP_FAIL: "USER_FAIL_SIGNUP"
};