/*!

=========================================================
* Page Public
=========================================================

*/
import React from "react";
// javascript plugin used to create scrollbars on windows
// import PerfectScrollbar from "perfect-scrollbar";

// import DemoNavbar from "components/Navbars/DemoNavbar.jsx";
// import PublicNavbar from "components/Navbars/PublicNavbar.jsx";
import MenuNavbar from "components/Navbars/MenuNavbar.jsx";

// import LoginPage from "views/Login/ValidateLoginForm.jsx"
import LoginPage from "views/Login/Login.jsx"
class LoginLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    return (
      <div className="wrapper">
        <div className="main-panel" >
          {/* <PublicNavbar {...this.props} /> */}
          <MenuNavbar {...this.props} />
          <LoginPage />
        </div>
      </div>
    );
  }
}

export default LoginLayout;
