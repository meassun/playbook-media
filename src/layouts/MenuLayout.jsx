/*!

=========================================================
* Page Admin
=========================================================

*/
import React from "react";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";

// import HomeNavbar from "components/Navbars/HomeNavbar.jsx";
import MenuNavbar from "components/Navbars/MenuNavbar.jsx";
import HomePage from "views/Home/Home.jsx"
import PublicPage from "views/Public.jsx"
import { connect } from 'react-redux';
import { isUserLogin } from "../redux/actions/UserAction";
var ps;

class MenuLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.mainPanel = React.createRef();
    this.props.checkIfUserLogin();
  }
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.mainPanel.current);
      document.body.classList.toggle("perfect-scrollbar-on");
    }
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
      document.body.classList.toggle("perfect-scrollbar-on");
    }
  }
  componentDidUpdate(e) {
    if (e.history.action === "PUSH") {
      this.mainPanel.current.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
    }
  }
  render() {
    return (
      <div className="wrapper">
        <div className="main-panel" ref={this.mainPanel} >
          <MenuNavbar {...this.props} />
          {this.props.isAuthenticated ? (
            <HomePage/>
          ) : (
            <PublicPage/>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.user.token != null
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkIfUserLogin: () => dispatch(isUserLogin())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuLayout);
