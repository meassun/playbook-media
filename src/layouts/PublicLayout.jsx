/*!

=========================================================
* Page Public
=========================================================

*/
import React from "react";
// javascript plugin used to create scrollbars on windows
// import PerfectScrollbar from "perfect-scrollbar";

// import DemoNavbar from "components/Navbars/DemoNavbar.jsx";
import PublicNavbar from "components/Navbars/PublicNavbar.jsx";
import { Route, Switch } from "react-router-dom";
import routes from "routes.js";

class PublicLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    return (
      <div className="wrapper">
        <div className="main-panel" >
          <PublicNavbar {...this.props}/>
          <Switch>
            {routes.map((prop, key) => {
              return (
                <Route
                  path={prop.layout + prop.path}
                  component={prop.component}
                  key={key}
                />
              );
            })}
          </Switch>
        </div>
      </div>
    );
  }
}

export default PublicLayout;
