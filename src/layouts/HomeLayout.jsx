/*!

=========================================================
* Page Admin
=========================================================

*/
import React from "react";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";

import HomeNavbar from "components/Navbars/HomeNavbar.jsx";
import HomePage from "views/Home/Home.jsx"
// import MessagePage from "views/Chats/Messages.jsx"
var ps;

class HomeLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.mainPanel = React.createRef();
  }
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.mainPanel.current);
      document.body.classList.toggle("perfect-scrollbar-on");
    }
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
      document.body.classList.toggle("perfect-scrollbar-on");
    }
  }
  componentDidUpdate(e) {
    if (e.history.action === "PUSH") {
      this.mainPanel.current.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
    }
  }
  render() {
    return (
      <div className="wrapper">
        <div className="main-panel" ref={this.mainPanel} >
          <HomeNavbar {...this.props} />
          <HomePage/>
          {/* <MessagePage /> */}
        </div>
      </div>
    );
  }
}

export default HomeLayout;
