import React from "react";
// import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss?v=1.1.0";
import "assets/demo/demo.css";
import "perfect-scrollbar/css/perfect-scrollbar.css";

import AdminLayout from "./layouts/AdminLayout.jsx";
import { connect } from "react-redux";
import { isUserLogin } from "redux/actions/UserAction";
import MenuLayout from "./layouts/MenuLayout.jsx";
import LoginLayout from "./layouts/LoginLayout.jsx"
import SignupLayout from "./layouts/SignupLayout.jsx";
import LockScreen from "./views/LockScreen/Lock.jsx"
import Error404 from './views/Error/Error404';
const hist = createBrowserHistory();

function App(props) {
	props.checkIfUserLogin();
	return (
		<Router history={hist}>
			<Switch>
				<Route exact={true} path="/" render={props => <MenuLayout {...props} />} />
				<Route path="/home" render={props => <MenuLayout {...props} />} />
				<Route path="/login" render={props => <LoginLayout {...props} />} />
				<Route path="/signup" render={props => <SignupLayout {...props} />} />
				<Route path="/admin" render={props => <AdminLayout {...props} />} />
				<Route path="/lock" render={props => <LockScreen {...props} />} />
				<Route path="*" render={props => <Error404 {...props} />} />

			</Switch>
		</Router>
	);
}

const mapStateToProps = (state) => {
	return {
		isAuthenticated: state.user.token != null
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		checkIfUserLogin: () => dispatch(isUserLogin())
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
